function limitFunctionCallCount(cb, n) {
    
    const invoker = () => {
        if (n > 0) {
            console.log(cb(n));
            n--;
        }
        else { 
            return console.log(null); 
        }
    }
    return invoker;
}

module.exports = limitFunctionCallCount;
