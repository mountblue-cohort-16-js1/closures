function cacheFunction(cb) {
    
    let cache = {};
    
    const cachedInvoker = argument => {

        if (argument in cache) {
            return `random number for ${argument} is cached as ${cache[argument]}`;
        }
        else {
            cache[argument] = Math.random()*10;
            return cb(cache, argument);
        }
    }
    return cachedInvoker;
}

module.exports = cacheFunction;
