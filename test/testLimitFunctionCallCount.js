const limitFunctionCallCount = require("../limitFunctionCallCount.js");
const cb = function(n) { return n + '-invocation';}
const invoker = limitFunctionCallCount(cb, 6);

invoker();
invoker();
invoker();
invoker();
invoker();
invoker();
invoker();
invoker();
