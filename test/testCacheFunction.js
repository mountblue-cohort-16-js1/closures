const cacheFunction = require("../cacheFunction");
const cb = (cache, argument) => { return `cb invoked for ${argument} as random number ${cache[argument]}`;}
const cachedInvoker = cacheFunction(cb);

console.log(cachedInvoker('firstRun'));
console.log(cachedInvoker('secondRun'));
console.log(cachedInvoker('thirdRun'));
console.log(cachedInvoker('secondRun'));
console.log(cachedInvoker('secondRun'));
console.log(cachedInvoker('thirdRun'));
