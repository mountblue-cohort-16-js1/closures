function counterFactory() {

    let counter = 0;

    const increment = () => { return counter += 1; }

    const decrement = () => { return counter -= 1; }

    const objects = { increment, decrement };
    return objects;
}

module.exports = counterFactory;
